import json
import sys
import networkx as nx  # type: ignore
from collections import defaultdict

if len(sys.argv) < 2:
    filename = "rf.json"
else:
    filename = sys.argv[1]

rawdata = json.loads(open(filename).read())
years = rawdata["data"]["years"]
artists = rawdata["data"]["artists"]

years_artists: defaultdict[int, list[str]] = defaultdict(list)
for artist in artists:
    artist_name = artist["data"]["Artist"]
    if isinstance(artist["data"]["Years"], str):
        artist_years = [int(y) for y in artist["data"]["Years"].split(", ")]
    else:
        # ...because some times "Years" is just an int
        artist_years = [int(artist["data"]["Years"])]
    for year in artist_years:
        years_artists[int(year)].append(artist_name)


def make_graph():
    graph = nx.Graph()
    for year, artists_ in years_artists.items():
        for artist in artists_:
            graph.add_node(artist)
            for otherartist in artists_:
                graph.add_edge(artist, otherartist, year=year)

    return graph


graph = make_graph()


def gnags_factor(artist):
    path = nx.shortest_path(graph, artist, "Gnags")
    result = len(path) - 2
    print(f"Gnags factor: {result}")
    for oneartist, anotherartist in zip(path[:-1], path[1:]):
        print(
            f"{oneartist} and {anotherartist} both played Roskilde {graph[oneartist][anotherartist]['year']}"
        )
    return result


while True:
    gnags_factor(input("Artist> "))
