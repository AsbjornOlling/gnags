{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  packages = with pkgs; [
    python311
    python311Packages.networkx

    # needed for networkx visualizations
    python311Packages.matplotlib
    python311Packages.scipy
    python311Packages.numpy
  ];
}
