{
  description = "erdos but gnags";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

        py = pkgs.python311.withPackages (pypkgs: [ pypkgs.networkx ]);

        data = ./rf.json;

        runscript = pkgs.writeShellScriptBin "gnags" "${py}/bin/python ${./gnags.py} ${data}";
      in {
        packages.default = runscript;
        apps.default = {
          type = "app";
          program = "${runscript}/bin/gnags";
        };
      });
}
